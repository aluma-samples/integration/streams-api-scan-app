# Streams API Sample Scan Application
This repository contains the code for a sample "scan application" intended to demonstrate how to create a real scan application that integrates with the Aluma Document Streams API.

The application is a web app that you can run locally and interact with in a browser.

The application:
 * Emulates a "scan" (select a file from the local computer)
 * Allows the user to select a Project to process the file with
 * Implements a custom validation user interface
 * Implements its own export functionality

# Running the application
You must have node.js and the npm package manager installed to run the application. See [here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) for instructions on how to install these if you don't have them.

You will also need a valid set Aluma API client credentials. You can use an existing API client or create a new one in the [Aluma dashboard](https://dashboard.aluma.io).

1. From a terminal window, in the root folder of this repository, run `npm install` to install the package dependencies.

2. Run `npm run serve` to build and serve the web application

3. Open a browser and navigate to `http://localhost:8080`.

4. Copy an API client ID and secret into the fields presented on the UI and click Save. The credentials are stored securely in a browser cookie. You may need to refresh the page before you see the main user interface.

5. Select one of the projects and upload a file to simulate a scan. Once the document is ready for validation a validation user interface will be presented. 

6. Edit any data you want to and click Save. Once the document is ready for export the data to be exported will be presented in the UI (the application does not actually do the export).


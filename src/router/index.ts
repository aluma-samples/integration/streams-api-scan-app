import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import ReviewDocument from "@/views/Validate.vue";

import Scan from "@/views/Scan.vue";
import ExportDocument from "@/views/Export.vue";

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace RouteNames {
  export const Scan = 'Scan';
  export const Validate = 'Validate';
  export const Export = 'Export';
}

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: RouteNames.Scan,
    component: Scan
  },
  {
    path: '/validate/:streamId',
    name: RouteNames.Validate,
    component: ReviewDocument
  },
  {
    path: '/export/:streamId',
    name: RouteNames.Export,
    component: ExportDocument
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Scan.vue')
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router

export namespace DocumentExtraction {
    export interface Area {
        top: number;
        left: number;
        bottom: number;
        right: number;
        page_number: number;
    }

    export interface Result {
        text: string;
        value?: any;
        rejected: boolean;
        reject_reason: string;
        proximity_score: number;
        match_score: number;
        text_score: number;
        areas: Area[];
    }

    export interface FieldResult {
        field_name: string;
        rejected: boolean;
        reject_reason: string;
        result: Result;
        alternatives?: Result[];
        tabular_results?: any;
    }

    export interface Page {
        page_number: number;
        width: number;
        height: number;
    }

    export interface Document {
        page_count: number;
        pages: Page[];
    }

    export interface ExtractionResult {
        field_results: FieldResult[];
        document: Document;
    }

    export interface ExtractionForMarking {
        areas: Area[];
        bookmarks: Bookmark[];
    }

    export interface Area {
        top: number;
        left: number;
        bottom: number;
        right: number;
        page_number: number;
    }

    export interface Bookmark {
        text: string;
        page_number: number;
    }
}

export function mapFieldResult<T>(
    field: DocumentExtraction.FieldResult,
    transform: (
        r: DocumentExtraction.Result,
        field: DocumentExtraction.FieldResult,
        index: number
    ) => T
): T[] {
    // helper that determines whether a field's alternative results should be
    // included
    function shouldOutputAlternatives(): boolean {
        if (!field.rejected) {
            return true;
        }

        // Field is rejected
        return field.reject_reason === "MultipleResults";
    }

    let results: T[] = [];

    if (field.result == null) {
        return results;
    }

    results = [transform(field.result, field, 0)];

    if (shouldOutputAlternatives() && field.alternatives) {
        results.push(
            ...field.alternatives.map((r, i) => transform(r, field, i + 1))
        );
    }

    return results;
}
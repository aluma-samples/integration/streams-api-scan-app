import type {HttpClient} from "../../http/httpClient";
import {CreateFileCollectionResponse} from "@/services/fileCollectionResponse";

export class FileCollectionClient {
    private readonly http: HttpClient;

    constructor(http: HttpClient) {
        this.http = http;
    }

    async create(fileContent: Blob): Promise<CreateFileCollectionResponse> {
        const response = await this.http.request({
            method: "POST",
            url: "/file_collections",
            headers: {
                "Content-Type": "application/pdf",
            },
            data: fileContent
        });

        return response.data as CreateFileCollectionResponse
    }
}

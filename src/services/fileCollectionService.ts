import type {FileCollectionClient} from "./fileCollectionClient";
import {CreateFileCollectionResponse} from "@/services/fileCollectionResponse";

export class FileCollectionService {
    private fileCollectionClient: FileCollectionClient;

    constructor(documentsClient: FileCollectionClient) {
        this.fileCollectionClient = documentsClient;
    }

    async create(fileContent: Blob): Promise<CreateFileCollectionResponse> {
        return await this.fileCollectionClient.create(fileContent);
    }
}

import type {HttpClient} from "../../http/httpClient";
import {GetProjectsResponse} from "@/services/projectResponse";

export class ProjectClient {
    private readonly http: HttpClient;

    constructor(http: HttpClient) {
        this.http = http;
    }

    async getAll(): Promise<GetProjectsResponse> {
        const response = await this.http.request({
            method: "GET",
            url: "/projects",
        });

        return response.data as GetProjectsResponse
    }
}

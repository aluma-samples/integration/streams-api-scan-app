import {ProjectClient} from "@/services/projectClient";
import {GetProjectsResponse} from "@/services/projectResponse";

export class ProjectService {
    private projectClient: ProjectClient;

    constructor(projectClient: ProjectClient) {
        this.projectClient = projectClient;
    }

    async getAll(): Promise<GetProjectsResponse> {
        return await this.projectClient.getAll();
    }
}

import axios from "axios";
import {
    AlumaResponseChecker,
    AuthenticatingHttpClient,
    AxiosHttpClient,
    ResponseCheckingHttpClient
} from "../../http/httpClient";

import {CachingTokenRetriever} from "../../http/tokenRetriever";
import {FileCollectionService} from "@/services/fileCollectionService";
import {FileCollectionClient} from "@/services/fileCollectionClient";
import {useCookies} from "vue3-cookies";
import {StreamService} from "@/services/streamService";
import {StreamClient} from "@/services/streamClient";
import {ProjectService} from "@/services/projectService";
import {ProjectClient} from "@/services/projectClient";

const {cookies} = useCookies();
const api_url = "https://api.aluma.io";
const axiosInstance = axios.create({
    baseURL: api_url
});
console.log("created axios instance");

const axiosHttpClient = new AxiosHttpClient(
    axiosInstance
);
console.log("created axios http client");

const http = new ResponseCheckingHttpClient(
    new AlumaResponseChecker(),
    new AuthenticatingHttpClient(
        new CachingTokenRetriever(axiosHttpClient, cookies.get("clientId"), cookies.get("clientSecret")),
        axiosHttpClient
    ),
);

export const fileCollectionService = new FileCollectionService(new FileCollectionClient(http));
export const streamService = new StreamService(new StreamClient(http));
export const projectService = new ProjectService(new ProjectClient(http));


export const doTokenValidation = async (): Promise<boolean> => {
    const tokenRetriever = new CachingTokenRetriever(axiosHttpClient, cookies.get("clientId"), cookies.get("clientSecret"))

    // if token exists return true if not return false
    return !!await tokenRetriever.retrieveToken()
}

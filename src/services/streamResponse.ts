export type StartClientActionResponse = ValidationStartData | ExportStartData
export type CompleteClientActionResponse = ValidateCompleteData | ExportCompleteData

export interface ValidateCompleteData {
    documents: ValidatedDocument[]
}

export interface ValidatedDocument {
    document_type: string
    fields: ValidatedField[]
}

export interface ValidatedField {
    name: string
    value: string
}

export interface CreateStreamResponse {
    id: string;
    file_collection_id: string
    project_id: string
    created_at: string
    status: string
}

export interface Document {
    document_type: string;
    fields: FieldDefinition[]
    all_document_types: {
        [key: string]: DocumentType;
    };
}

export interface ValidationStartData {
    documents: Document[]
    document_types: DocumentType[]
}

export interface FieldDefinition {
    name: string
    settings: any
    tabular_results: any
    rejected: boolean
    rejected_reason: string
    results: Result[]
}

export interface Result {
    text: string
    value: any
    rejected: boolean
    reject_reason: string
    areas: Area[]
}

export interface Area {
    top: string
    left: string
    bottom: string
    right: string
    page_number: number
}

export interface Field {
    name: string
    settings: any
}

export interface DocumentType {
    name: string
    fields: Field[];
}

export interface ExportStartData {
    documents: [
        { files: OutputFile[] }
    ]
}

export interface OutputFile {
    folder: string
    filename: string
    content: string
}

export interface StreamResponse {
    id: string
    state: string
    client_action: string
}

export interface ExportCompleteData {
    processed_by: string
}

export type ClientAction = "validation" | "export"
